FROM node:16-alpine

WORKDIR /usr/src/myapp

COPY ./ ./

EXPOSE 3000

CMD ["npm", "start"]