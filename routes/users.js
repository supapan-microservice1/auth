const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const { QueryTypes } = require('sequelize');
const jwt = require('jsonwebtoken');

const model = require('../models/index');
const passportJWT = require('../middlewares/passport-jwt');

//const { QueryTypes } = require('sequelize/types');

//get user profile (คนที่กำลัง login อยู่)
/* /api/v1/users/profile */
router.get('/profile',[passportJWT.isLogin], async function(req, res, next) {
  const user = await model.User.findByPk(req.user.user_id);
  return res.status(200).json({
    user: {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      created_at: user.created_at
    }
  });
});

/* GET users listing. */
/* /api/v1/users */
router.get('/', async function(req, res, next) {
  // const users = await model.User.findAll();
 // const users = await model.User.findAll({
    // attributes: ['id', 'fullname']
  //  attributes: {exclude: ['password']},
  //  order: [['id','desc']]
  //});
  
  const sql = 'SELECT id,fullname,email FROM `users` ORDER BY id DESC';
  const users = await model.sequelize.query(sql,{
    type: QueryTypes.SELECT
  });

  const totalUsers = await model.User.count();

  return res.status(200).json({
    total: totalUsers,
    data: users
  });
});
/* /api/v1/users/register */
router.post('/register', async function(req, res, next) {
  const {fullname,email,password} = req.body;

//1.เช็คอีเมล์ว่าซ้ำกันหรือไม่
  const user = await model.User.findOne({ where: { email: email } });
  if (user !== null) {
    return res.status(400).json({messege: 'User exists.'});
  }
//2.เข้ารหัส pwd
  const passwordHash = await argon2.hash(password); 

//3.บันทึกลงตาราง users
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passwordHash 
  });

  return res.status(201).json({
    user: {
      id: newUser.id,
      fullname: newUser.fullname
    },
    messege:'register successfully'
  });
});

// Login
/* /api/v1/users/login */
router.post('/login', async function(req, res, next) {
  const {email,password} = req.body;

//1.เช็คอีเมล์ว่ามีระบบหรือไม่
  const user = await model.User.findOne({ where: { email: email } });
  if (user === null) {
    return res.status(404).json({messege: 'User does not exist.'});
  }
//2.ตรวจสอลรหัส pwd ว่าถูกต้องตรงกับข้อมูลในฐานข้อมูลหรือไม่
  const isValid = await argon2.verify(user.password, password); 
  if (!isValid) {
    return res.status(401).json({messege: 'Password is not valid.'});
  }

//3. สร้าง Token
  const token = jwt.sign({user_id: user.id },process.env.JWT_KEY,{ expiresIn: '7d'});

  return res.status(200).json({
    message: 'Login successfully.',
    access_token: token 
  });
});


module.exports = router;
